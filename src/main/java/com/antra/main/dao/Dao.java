package com.antra.main.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.antra.main.entity.LoginEntity;

public interface Dao extends JpaRepository<LoginEntity, String> {
}