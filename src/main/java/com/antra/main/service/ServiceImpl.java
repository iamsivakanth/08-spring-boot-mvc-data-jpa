package com.antra.main.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.main.dao.Dao;
import com.antra.main.entity.LoginEntity;

@Service
public class ServiceImpl implements MyService {

	@Autowired
	Dao dao;

	@Override
	public boolean verifyUser(String uname, String pword) {
		Optional<LoginEntity> op = dao.findById(uname);
		
		if (op.isPresent()) {
			LoginEntity lg = op.get();
			if (lg.getActiveuser() == 1) {
				if (lg.getPword().equals(pword)) {
					return true;
				} else
					return false;
			} else
				return false;
		} else
			return false;
	}
}